# By Vay3t
# from https://pentesting.mrw0l05zyn.cl/explotacion/web/input-data-validation

from burp import IBurpExtender
from burp import IIntruderPayloadGeneratorFactory
from burp import IIntruderPayloadProcessor
from burp import IIntruderPayloadGenerator


# chars = '!@#$%^&~_-+=*.,:;\'"\|/?<>[{()}] '
# PAYLOADS = [bytearray(char, 'utf-8') for char in chars]

PAYLOADS = [
    bytearray(b"!"),
    bytearray(b"@"),
    bytearray(b"#"),
    bytearray(b"$"),
    bytearray(b"%"),
    bytearray(b"^"),
    bytearray(b"&"),
    bytearray(b"~"),
    bytearray(b"_"),
    bytearray(b"-"),
    bytearray(b"+"),
    bytearray(b"="),
    bytearray(b"*"),
    bytearray(b"."),
    bytearray(b","),
    bytearray(b":"),
    bytearray(b";"),
    bytearray(b"'"),
    bytearray(b'\\"'),
    bytearray(b'"'),
    bytearray(b"\\"),
    bytearray(b"|"),
    bytearray(b"/"),
    bytearray(b"?"),
    bytearray(b"<"),
    bytearray(b">"),
    bytearray(b"["),
    bytearray(b"{"),
    bytearray(b"("),
    bytearray(b")"),
    bytearray(b"]"),
    bytearray(b"}"),
    bytearray(b" "),
]


class BurpExtender(
    IBurpExtender, IIntruderPayloadGeneratorFactory, IIntruderPayloadProcessor
):
    #
    # implement IBurpExtender
    #

    def registerExtenderCallbacks(self, callbacks):
        # obtain an extension helpers object
        self._helpers = callbacks.getHelpers()

        # set our extension name
        callbacks.setExtensionName("Special Chars Payloads Intruder")

        # register ourselves as an Intruder payload generator
        callbacks.registerIntruderPayloadGeneratorFactory(self)

    #
    # implement IIntruderPayloadGeneratorFactory
    #

    def getGeneratorName(self):
        return "Special Chars Payloads"

    def createNewInstance(self, attack):
        # return a new IIntruderPayloadGenerator to generate payloads for this attack
        return IntruderPayloadGenerator()


#
# class to generate payloads from a simple list
#


class IntruderPayloadGenerator(IIntruderPayloadGenerator):
    def __init__(self):
        self._payloadIndex = 0

    def hasMorePayloads(self):
        return self._payloadIndex < len(PAYLOADS)

    def getNextPayload(self, baseValue):
        payload = PAYLOADS[self._payloadIndex]
        self._payloadIndex = self._payloadIndex + 1

        return payload

    def reset(self):
        self._payloadIndex = 0
