# My Intruder Paylaods
My Python and maybe wordlists for Bupsuite Intruder

## List of Payloads
- `extensions-list.py`: List of extensions (for **Community**)
- `format-string.py`: Format string (for **Community**)
- `hex-payloads.py`: List of hex values from %00 to %ff (for **Community** and **Pro**)
    - `doble-hex-payloads.py`: List of doble hex payloads (for **Community** and **Pro**)
- `ipspoofing-headers.py`: List of IP spoofing headers. Example: X-Forwarded-For header. (for **Community** and **Pro**)
- `naughtly-base64.py`: List of Naughtly strings in base64 encode (for **Community** and **Pro**)
- `special-chars.py`: List of special chars payloads (for **Community** and **Pro**)
- `template-injection.py`: Template injection (for **Community**)
- `verbs-list.py`: List of verbs for HTTP requests. (for **Community**)
