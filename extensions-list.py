# By Vay3t
# from https://github.com/JoshuaProvoste/Common-HTTP-Methods

from burp import IBurpExtender
from burp import IIntruderPayloadGeneratorFactory
from burp import IIntruderPayloadProcessor
from burp import IIntruderPayloadGenerator

LIST_EXTENSIONS = """
a
asp
aspx
backup
bak
c
cfg
cfm
cfml
class
com
conf
cpp
dat
data
db
dbc
dbf
debug
dev
dhtml
dll
doc
docx
dot
exe
gif
gz
htm
html
htr
htw
htx
ida
idc
idq
inc
ini
jar
java
jhtml
jpg
js
jsp
log
lst
net
o
old
pdf
php
php3
php4
php5
phtm
phtml
pl
printer
prn
py
rb
reg
rtf
save
sgml
sh
shtm
shtml
source
src
stm
sys
tar
tar.gz
temp
test
text
tgz
tmp
tst
txt
xls
xlsx
xml
zip
~
"""

PAYLOADS = []

for element in LIST_EXTENSIONS.split("\n"):
    if element != "":
        PAYLOADS.append(bytearray(element))



class BurpExtender(IBurpExtender, IIntruderPayloadGeneratorFactory, IIntruderPayloadProcessor):

    #
    # implement IBurpExtender
    #
    
    def registerExtenderCallbacks(self, callbacks):
        # obtain an extension helpers object
        self._helpers = callbacks.getHelpers()
        
        # set our extension name
        callbacks.setExtensionName("Extensions List Intruder")
        
        # register ourselves as an Intruder payload generator
        callbacks.registerIntruderPayloadGeneratorFactory(self)

    #
    # implement IIntruderPayloadGeneratorFactory
    #
    
    def getGeneratorName(self):
        return "Extensions List"

    def createNewInstance(self, attack):
        # return a new IIntruderPayloadGenerator to generate payloads for this attack
        return IntruderPayloadGenerator()

#
# class to generate payloads from a simple list
#

class IntruderPayloadGenerator(IIntruderPayloadGenerator):
    def __init__(self):
        self._payloadIndex = 0

    def hasMorePayloads(self):
        return self._payloadIndex < len(PAYLOADS)

    def getNextPayload(self, baseValue):
        payload = PAYLOADS[self._payloadIndex]
        self._payloadIndex = self._payloadIndex + 1

        return payload

    def reset(self):
        self._payloadIndex = 0
