# By Vay3t
# from https://github.com/JoshuaProvoste/IP-Spoofing-Headers

from burp import IBurpExtender
from burp import IIntruderPayloadGeneratorFactory
from burp import IIntruderPayloadProcessor
from burp import IIntruderPayloadGenerator


LIST_HEADERS = """
Access-Control-Allow-Origin
CF-Connecting-IP
Client-Ip
Fastly-Client-IP
Forwarded
Forwarded-For-IP
Origin
True-Client-IP
Via
X-Client-IP
X-Cluster-Client-Ip
X-Custom-IP-Authorization
X-Forward-For
X-Forwarded
X-Forwarded-By
X-Forwarded-For
X-Forwarded-For-IP
X-Forwarded-For-Original
X-Forwarded-Host
X-Forwarded-Port
X-Forwarded-Proto
X-Forwarded-Server
X-Host
X-Original-URL
X-Originating-IP
X-ProxyUser-Ip
X-Real-IP
X-Remote-Addr
X-Remote-Host
X-Remote-IP
X-Server-Name
"""

PAYLOADS = []

for element in LIST_HEADERS.split("\n"):
    if element != "":
        PAYLOADS.append(bytearray(element))


class BurpExtender(IBurpExtender, IIntruderPayloadGeneratorFactory, IIntruderPayloadProcessor):

    #
    # implement IBurpExtender
    #
    
    def registerExtenderCallbacks(self, callbacks):
        # obtain an extension helpers object
        self._helpers = callbacks.getHelpers()
        
        # set our extension name
        callbacks.setExtensionName("IP-Spoofing Headers Intruder")
        
        # register ourselves as an Intruder payload generator
        callbacks.registerIntruderPayloadGeneratorFactory(self)

    #
    # implement IIntruderPayloadGeneratorFactory
    #
    
    def getGeneratorName(self):
        return "IP-Spoofing Headers"

    def createNewInstance(self, attack):
        # return a new IIntruderPayloadGenerator to generate payloads for this attack
        return IntruderPayloadGenerator()

#
# class to generate payloads from a simple list
#

class IntruderPayloadGenerator(IIntruderPayloadGenerator):
    def __init__(self):
        self._payloadIndex = 0

    def hasMorePayloads(self):
        return self._payloadIndex < len(PAYLOADS)

    def getNextPayload(self, baseValue):
        payload = PAYLOADS[self._payloadIndex]
        self._payloadIndex = self._payloadIndex + 1

        return payload

    def reset(self):
        self._payloadIndex = 0
